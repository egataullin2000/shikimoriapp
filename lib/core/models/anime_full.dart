import 'package:flutter/foundation.dart';

import 'anime_preview.dart';

class AnimeFull extends AnimePreview {
  final String posterUrl;
  final Status status;
  final int episodesCount, episodesAired;
  final DateTime airedOn, releasedOn;
  final int duration;
  final String description;
  final bool isAnons;
  final String nextEpisodeAt;
  final List<String> genres;
  final List<String> studios;
  final List<Video> videos;
  final List<Screenshot> screenshots;
  final List<Character> characters;

  const AnimeFull(
      {@required id,
      @required title,
      @required posterPreviewUrl,
      @required kind,
      @required score,
      @required this.posterUrl,
      @required this.status,
      @required this.episodesCount,
      this.episodesAired,
      this.airedOn,
      this.releasedOn,
      this.duration,
      @required this.description,
      @required this.isAnons,
      this.nextEpisodeAt,
      @required this.genres,
      @required this.studios,
      this.videos,
      @required this.screenshots,
      this.characters})
      : super(
            id: id,
            title: title,
            posterPreviewUrl: posterPreviewUrl,
            kind: kind,
            score: score);

  factory AnimeFull.fromJson(Map<String, dynamic> json) => AnimeFull(
      id: json['id'],
      title: Title(json['russian'], json['name']),
      posterPreviewUrl: 'https://shikimori.one${json['image']['preview']}',
      kind: (json['kind'] as String).toUpperCase(),
      score: double.parse(json['score']),
      posterUrl: 'https://shikimori.one${json['image']['original']}',
      status: Status.valueOf(json['status']),
      episodesCount: json['episodes'],
      episodesAired: json['episodes_aired'],
      airedOn: DateTime.parse(json['aired_on']),
      releasedOn: json['released_on'] == null
          ? null
          : DateTime.parse(json['released_on']),
      duration: json['duration'],
      description:
          (json['description'] as String).replaceAll(RegExp(r'\[[^]*?]'), ''),
      isAnons: json['anons'],
      nextEpisodeAt: json['next_episode_at'],
      genres: json['genres']
          .map((e) => e['russian'].toLowerCase())
          .toList(growable: false)
          .cast<String>(),
      studios: json['studios']
          .map((e) => e['filtered_name'])
          .toList(growable: false)
          .cast<String>(),
      videos: json['videos']
          .map((e) => Video.fromJson(e))
          .toList(growable: false)
          .cast<Video>(),
      screenshots: json['screenshots']
          .map((e) => Screenshot.fromJson(e))
          .toList(growable: false)
          .cast<Screenshot>());
}

class Status {
  static const ANONS = const Status._('anons', 'анонс');
  static const ONGOING = const Status._('ongoing', 'выходит');
  static const RELEASED = const Status._('released', 'вышло');

  final String _valueEn, _valueRu;

  const Status._(this._valueEn, this._valueRu);

  String get en => _valueEn;

  String get ru => _valueRu;

  static List<Status> get values => [RELEASED];

  static Status valueOf(String name) =>
      values.where((status) => status._valueEn == name).first;

  @override
  String toString() {
    return _valueEn;
  }
}

class Video {
  final String title;
  final String url;
  final String posterSrc;

  const Video(this.title, this.url, this.posterSrc);

  factory Video.fromJson(Map<String, dynamic> json) =>
      Video(json['name'], json['player_url'], json['image_url']);
}

class Screenshot {
  final String url, previewUrl;

  const Screenshot(this.url, this.previewUrl);

  factory Screenshot.fromJson(Map<String, dynamic> json) =>
      Screenshot(
          'https://shikimori.one${json['original']}',
          'https://shikimori.one${json['preview']}');
}

class Character {
  final String name;
  final String image;

  const Character({@required this.name, @required this.image});

  factory Character.fromJson(Map<String, dynamic> json) =>
      Character(
          name: 'https://shikimori.one${json['russian']}',
          image: 'https://shikimori.one${json['preview']}');
}
