import 'package:flutter/foundation.dart';

class AnimePreview {
  final int id;
  final Title title;
  final String posterPreviewUrl;
  final String kind;
  final double score;

  const AnimePreview({
    @required this.id,
    @required this.title,
    @required this.posterPreviewUrl,
    @required this.kind,
    @required this.score,
  });

  factory AnimePreview.fromJson(Map<String, dynamic> json) {
    return AnimePreview(
      id: json['id'],
      title: Title(json['russian'], json['name']),
      posterPreviewUrl: 'https://shikimori.one${json['image']['preview']}',
      kind: (json['kind'] as String).toUpperCase(),
      score: double.parse(json['score']),
    );
  }
}

class Title {
  final String ru, en;

  Title(this.ru, this.en);
}

class Studio {
  final String name, imageSrc;

  Studio(this.name, this.imageSrc);
}
