import 'dart:ui';

import 'package:flutter/widgets.dart';

class AppLocalizations {
  AppLocalizations(Locale locale) : languageCode = locale.languageCode;

  final String languageCode;

  static AppLocalizations of(BuildContext context) =>
      Localizations.of<AppLocalizations>(context, AppLocalizations);

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'catalog.title': 'Catalog',
      'message.loading': 'Loading',
      'message.error': 'Sorry, an error has occurred',
      'anime_details.rating': 'Rating',
      'anime_details.title_en': 'Title (en)',
      'anime_details.kind': 'Kind',
      'anime_details.genres': 'Genres',
      'anime_details.status': 'Status',
      'anime_details.episodes_count': 'Episodes',
      'anime_details.publishing_dates': 'Release dates',
      'anime_details.publishing_from': 'from',
      'anime_details.publishing_to': 'to',
      'anime_details.studio': 'Studio',
      'anime_details.screenshots': 'Screenshots',
      'anime_details.videos': 'Videos',
      'anime_details.characters': 'Characters',
      'anime_details.similar': 'Similar',
    },
    'ru': {
      'catalog.title': 'Каталог аниме',
      'message.loading': 'Загрузка',
      'message.error': 'Извините, произошла ошибка',
      'anime_details.rating': 'Рейтинг',
      'anime_details.title_en': 'Название (en)',
      'anime_details.kind': 'Тип',
      'anime_details.genres': 'Жанры',
      'anime_details.status': 'Статус',
      'anime_details.episodes_count': 'Серий',
      'anime_details.publishing_dates': 'Даты выхода',
      'anime_details.publishing_from': 'с',
      'anime_details.publishing_to': 'по',
      'anime_details.studio': 'Студия',
      'anime_details.screenshots': 'Кадры',
      'anime_details.videos': 'Видео',
      'anime_details.characters': 'Персонажи',
      'anime_details.similar': 'Похожее',
    },
  };

  Map<String, String> get localizedValue => _localizedValues[languageCode];

  String get animeCatalogTitle => localizedValue['catalog.title'];

  String get loadingMessage => localizedValue['message.loading'];

  String get errorMessage => localizedValue['message.error'];

  String get animeRating => localizedValue['anime_details.rating'];

  String get animeTitleEn => localizedValue['anime_details.title_en'];

  String get animeKind => localizedValue['anime_details.kind'];

  String get animeGenres => localizedValue['anime_details.genres'];

  String get animeStatus => localizedValue['anime_details.status'];

  String get animeEpisodesCount =>
      localizedValue['anime_details.episodes_count'];

  String get animePublishingDates =>
      localizedValue['anime_details.publishing_dates'];

  String get animePublishingFrom =>
      localizedValue['anime_details.publishing_from'];

  String get animePublishingTo => localizedValue['anime_details.publishing_to'];

  String get animeStudio => localizedValue['anime_details.studio'];

  String get animeScreenshots => localizedValue['anime_details.screenshots'];

  String get animeVideos => localizedValue['anime_details.videos'];

  String get animeCharacters => localizedValue['anime_details.characters'];

  String get animeSimilar => localizedValue['anime_details.similar'];
}
