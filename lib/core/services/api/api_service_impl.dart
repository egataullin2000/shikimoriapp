import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:shiki_app/core/models/anime_full.dart';
import 'package:shiki_app/core/models/anime_preview.dart';
import 'package:shiki_app/core/services/api/api_service.dart';

part 'api_service_impl.g.dart';

@RestApi(baseUrl: 'https://shikimori.one')
abstract class ApiServiceImpl extends ApiService {
  factory ApiServiceImpl() =>
      _ApiServiceImpl(Dio(BaseOptions(headers: {'User-Agent': 'ShikiApp'})));

  @override
  @GET("/api/animes")
  Future<List<AnimePreview>> getAnimeList({
    @required @Query('page') int page,
    @Query('limit') int limit = 10,
    @Query('order') String order = 'ranked',
  });

  @override
  @GET('/api/animes/{id}')
  Future<AnimeFull> getAnime(@Path() int id);
}
