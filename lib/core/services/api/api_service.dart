import 'package:flutter/foundation.dart';
import 'package:shiki_app/core/models/anime_full.dart';
import 'package:shiki_app/core/models/anime_preview.dart';

abstract class ApiService {
  Future<List<AnimePreview>> getAnimeList({
    @required int page,
    int limit = 10,
    String order = 'ranked',
  });

  Future<AnimeFull> getAnime(int id);
}