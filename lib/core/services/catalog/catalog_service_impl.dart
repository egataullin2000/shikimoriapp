import 'package:injector/injector.dart';
import 'package:shiki_app/core/models/anime_preview.dart';
import 'package:shiki_app/core/services/api/api_service.dart';

import 'catalog_service.dart';

class CatalogServiceImpl extends CatalogService {
  final ApiService _api;
  int _pages = 0;
  List<AnimePreview> _data = [];
  bool _hasNext = true;
  int lastRequested = 0;
  Future _loading;

  CatalogServiceImpl()
      : _api = Injector.appInstance.getDependency<ApiService>();

  @override
  int getDataSize() => _data.length;

  @override
  bool hasNext() => _hasNext;

  @override
  Future<AnimePreview> getAnime(int index) {
    return Future(() async {
      if (lastRequested < index) lastRequested = index;
      while (index >= _data.length) {
        if (_loading == null) _loadData();
        await _loading;
      }

      return _data[index];
    });
  }

  Future _loadData() {
    return _loading = _api.getAnimeList(page: ++_pages).then((value) {
      _data.addAll(value);
      _loading = lastRequested >= _data.length ? _loadData() : null;
    });
  }
}
