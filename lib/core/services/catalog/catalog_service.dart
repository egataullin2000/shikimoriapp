import 'package:shiki_app/core/models/anime_preview.dart';

abstract class CatalogService {
  int getDataSize();

  bool hasNext();

  Future<AnimePreview> getAnime(int index);
}
