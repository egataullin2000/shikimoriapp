import 'package:flutter/material.dart';
import 'package:injector/injector.dart';
import 'package:provider/provider.dart';
import 'package:shiki_app/core/localization/app_localizations.dart';
import 'package:shiki_app/core/localization/app_localizations_delegate.dart';
import 'package:shiki_app/core/services/api/api_service_impl.dart';
import 'package:shiki_app/core/services/catalog/catalog_service.dart';
import 'package:shiki_app/core/services/catalog/catalog_service_impl.dart';
import 'package:shiki_app/ui/anime_details/anime_details_model.dart';
import 'package:shiki_app/ui/anime_details/anime_details_page.dart';
import 'package:shiki_app/ui/home/home_model.dart';
import 'package:shiki_app/ui/home/home_page.dart';

import 'core/services/api/api_service.dart';

void main() {
  Injector injector = Injector.appInstance;
  injector.registerSingleton<ApiService>((_) => ApiServiceImpl());
  injector.registerSingleton<CatalogService>((_) => CatalogServiceImpl());

  return runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'ShikiApp',
        theme: ThemeData(
          brightness: Brightness.dark,
          fontFamily: 'OpenSans',
        ),
        localizationsDelegates: [AppLocalizationsDelegate()],
        supportedLocales: [const Locale('en'), const Locale('ru')],
        routes: {
          '/': (context) => ChangeNotifierProvider<HomeModel>(
                create: (_) => HomeModel(),
                child: HomePage(),
              ),
          AnimeDetailsPage.routeName: (context) =>
              ChangeNotifierProvider<AnimeDetailsModel>(
                create: (_) => AnimeDetailsModel(
                    ModalRoute.of(context).settings.arguments,
                    AppLocalizations.of(context)),
                child: AnimeDetailsPage(),
              ),
        },
      );
}
