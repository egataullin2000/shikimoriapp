import 'package:flutter/foundation.dart';
import 'package:injector/injector.dart';
import 'package:shiki_app/core/models/anime_preview.dart';
import 'package:shiki_app/core/services/catalog/catalog_service.dart';

class CatalogModel extends ChangeNotifier {
  final CatalogService _service;
  bool _hasError = false;

  CatalogModel()
      : _service = Injector.appInstance.getDependency<CatalogService>();

  bool get hasError => _hasError;

  int get dataSize => _service.getDataSize();

  Future<AnimePreview> getAnime(int index) {
    return _service.getAnime(index).catchError((_) {
      _hasError = hasError;
      notifyListeners();
    });
  }
}
