import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shiki_app/core/localization/app_localizations.dart';
import 'package:shiki_app/ui/anime_details/anime_details_page.dart';
import 'package:shiki_app/ui/catalog/catalog_model.dart';
import 'package:shiki_app/ui/widgets/anime_tile.dart';
import 'package:styled_widget/styled_widget.dart';

class CatalogPage extends StatelessWidget {
  const CatalogPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text(AppLocalizations.of(context).animeCatalogTitle),
            floating: true,
          ),
          ChangeNotifierProvider(
            create: (_) => CatalogModel(),
            child: _AnimeCatalog(),
          ),
        ],
      );
}

class _AnimeCatalog extends StatelessWidget {

  const _AnimeCatalog({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final model = Provider.of<CatalogModel>(context, listen: false);

    return SliverList(
      delegate: SliverChildBuilderDelegate((context, i) {
        return FutureBuilder(
            future: model.getAnime(i),
            builder: (context, snapshot) =>
                AnimeTile(
                    anime: snapshot.data,
                    onTap: () =>
                        Navigator.pushNamed(
                          context,
                          AnimeDetailsPage.routeName,
                          arguments: snapshot.data,
                        )));
      }),
    );
  }
}

class Error extends StatelessWidget {
  final Color _color;
  final String _message;

  Error(BuildContext context)
      : _color = Theme
      .of(context)
      .errorColor,
        _message = AppLocalizations
            .of(context)
            .errorMessage;

  @override
  Widget build(BuildContext context) =>
      Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(
            Icons.error_outline,
            size: 48,
            color: _color,
          ),
          Text(
            _message,
            style: TextStyle(
              fontSize: 16,
              color: _color,
            ),
          )
        ],
      ).padding(all: 16);
}
