import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:shiki_app/core/localization/app_localizations.dart';
import 'package:shiki_app/core/models/anime_preview.dart';
import 'package:shiki_app/ui/widgets/themed_shimmer.dart';
import 'package:styled_widget/styled_widget.dart';

import 'cached_image.dart';
import 'material_hero.dart';

class AnimeTile extends StatelessWidget {
  final AnimePreview anime;
  final GestureTapCallback onTap;

  AnimeTile({Key key, this.anime, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    Widget body = Theme(
      data: Theme.of(context).copyWith(
        textTheme: TextTheme(
          overline: textTheme.overline.copyWith(
            color: textTheme.overline.color.withOpacity(.6),
          ),
          headline6: textTheme.headline6.copyWith(
            color: textTheme.headline6.color.withOpacity(.87),
          ),
          subtitle2: textTheme.subtitle2.copyWith(
            color: textTheme.subtitle2.color.withOpacity(.6),
          ),
        ),
      ),
      child: _Body(anime: anime),
    );
    if (anime != null && onTap != null)
      body = InkWell(
        onTap: onTap,
        child: body,
      );

    return body.card(clipBehavior: Clip.antiAliasWithSaveLayer);
  }
}

class _Body extends StatelessWidget {
  static const double _posterSize = 96;

  final AnimePreview anime;

  const _Body({Key key, this.anime});

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      _buildPoster(context),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(children: [
            _buildKind(context).expanded(),
            const SizedBox(width: 16),
            _buildRating(context),
          ]),
          _buildTitle(context).padding(top: 2, bottom: 4),
          _buildSubtitle(context),
        ],
      ).padding(horizontal: 16).expanded()
    ]);
  }

  Widget _buildPoster(BuildContext context) {
    final theme = Theme.of(context);
    final placeholder = ThemedShimmer(width: _posterSize, height: _posterSize);

    return anime == null
        ? placeholder
        : Hero(
            tag: '${anime.id}_poster',
            child: CachedImage(
              src: anime.posterPreviewUrl,
              width: _posterSize,
              height: _posterSize,
              placeholder: (_, __) => placeholder,
              errorWidget: (_, __, ___) => SizedBox(
                width: _posterSize,
                height: _posterSize,
                child: Icon(
                  Icons.error_outline,
                  color: theme.errorColor,
                  size: 48,
                ),
              ),
            ),
    );
  }

  Widget _buildKind(BuildContext context) {
    Widget text = Text(
      (anime?.kind ?? AppLocalizations.of(context).loadingMessage)
          .toUpperCase(),
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: Theme.of(context).textTheme.overline,
    );

    return anime == null
        ? ThemedShimmer(child: text)
        : MaterialHero(tag: '${anime.id}_kind', child: text);
  }

  Widget _buildRating(BuildContext context) {
    Widget ratingItem = const Icon(Icons.star_border, color: Colors.amber);
    if (anime == null) ratingItem = ThemedShimmer(child: ratingItem);
    Widget rating = RatingBarIndicator(
      itemBuilder: (_, __) => ratingItem,
      itemCount: 5,
      itemSize: 16,
      unratedColor: Theme.of(context).disabledColor,
      rating: anime == null ? 0 : anime.score / 2,
    );

    return anime == null
        ? rating
        : Hero(tag: '${anime.id}_rating', child: rating);
  }

  Widget _buildTitle(BuildContext context) {
    Widget title = Text(
      anime?.title?.ru ?? AppLocalizations.of(context).loadingMessage,
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: Theme.of(context).textTheme.headline6,
    );

    return anime == null
        ? ThemedShimmer(child: title)
        : MaterialHero(tag: '${anime.id}_title_ru', child: title);
  }

  Widget _buildSubtitle(BuildContext context) {
    Widget subtitle = Text(
      anime?.title?.en ?? AppLocalizations.of(context).loadingMessage,
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: Theme.of(context).textTheme.subtitle2,
    );

    return anime == null
        ? ThemedShimmer(child: subtitle)
        : MaterialHero(tag: '${anime.id}_title_en', child: subtitle);
  }
}
