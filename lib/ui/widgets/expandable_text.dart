import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:styled_widget/styled_widget.dart';

class ExpandableText extends StatefulWidget {
  final String text;
  final int maxLines;
  final bool initiallyExpanded;
  final Duration animationDuration;
  final Widget Function(Widget text) textDecorator;
  final TextStyle style;
  final StrutStyle strutStyle;
  final TextAlign textAlign;
  final TextDirection textDirection;
  final Locale locale;
  final bool softWrap;
  final TextOverflow overflow;
  final double textScaleFactor;
  final String semanticsLabel;
  final TextWidthBasis textWidthBasis;
  final TextHeightBehavior textHeightBehavior;

  const ExpandableText({
    Key key,
    this.text,
    @required this.maxLines,
    this.initiallyExpanded = false,
    this.animationDuration = kThemeAnimationDuration,
    this.textDecorator,
    this.style,
    this.strutStyle,
    this.textAlign,
    this.textDirection = TextDirection.ltr,
    this.locale,
    this.softWrap,
    this.overflow = TextOverflow.ellipsis,
    this.textScaleFactor,
    this.semanticsLabel,
    this.textWidthBasis,
    this.textHeightBehavior,
  }) : super(key: key);

  @override
  _ExpandableTextState createState() => _ExpandableTextState();
}

class _ExpandableTextState extends State<ExpandableText>
    with SingleTickerProviderStateMixin {
  String text;
  bool isExpanded;

  @override
  void initState() {
    super.initState();
    isExpanded = widget.initiallyExpanded;
    text = widget.text;
  }

  @override
  Widget build(BuildContext context) {
    Widget text = Text(
      widget.text,
      style: widget.style,
      strutStyle: widget.strutStyle,
      textAlign: widget.textAlign,
      textDirection: widget.textDirection,
      locale: widget.locale,
      softWrap: widget.softWrap,
      overflow: isExpanded ? null : widget.overflow,
      textScaleFactor: widget.textScaleFactor,
      maxLines: isExpanded ? null : widget.maxLines,
      semanticsLabel: widget.semanticsLabel,
      textWidthBasis: widget.textWidthBasis,
      textHeightBehavior: widget.textHeightBehavior,
    );
    if (widget.textDecorator != null) text = widget.textDecorator(text);

    return LayoutBuilder(builder: (context, size) {
      final span = TextSpan(text: widget.text, style: widget.style);
      final tp = TextPainter(
        text: span,
        maxLines: widget.maxLines,
        textDirection: widget.textDirection,
      );
      tp.layout(maxWidth: size.maxWidth);

      return tp.didExceedMaxLines
          ? Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              AnimatedSize(
                duration: widget.animationDuration,
                vsync: this,
                alignment: Alignment.topLeft,
                child: text,
              ),
              Align(
                  alignment: Alignment.bottomRight,
                  child: IconButton(
                    icon: Icon(isExpanded
                        ? Icons.keyboard_arrow_up
                        : Icons.keyboard_arrow_down),
                    onPressed: () => setState(() => isExpanded = !isExpanded),
                  ))
            ])
          : text.padding(bottom: 16);
    });
  }
}
