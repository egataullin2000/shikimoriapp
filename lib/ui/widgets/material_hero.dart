import 'package:flutter/material.dart';

class MaterialHero extends Hero {
  MaterialHero({
    Key key,
    @required String tag,
    CreateRectTween createRectTween,
    HeroFlightShuttleBuilder flightShuttleBuilder,
    HeroPlaceholderBuilder placeholderBuilder,
    transitionOnUserGestures = false,
    @required Widget child,
  }) : super(
            key: key,
            tag: tag,
            createRectTween: createRectTween,
            flightShuttleBuilder: flightShuttleBuilder,
            placeholderBuilder: placeholderBuilder,
            transitionOnUserGestures: transitionOnUserGestures,
            child: Material(color: Colors.transparent, child: child));
}
