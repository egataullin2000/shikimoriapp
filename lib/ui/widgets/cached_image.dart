import 'dart:io' show Platform;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';

class CachedImage extends StatelessWidget {
  final String src;
  final ImageWidgetBuilder imageBuilder;
  final PlaceholderWidgetBuilder placeholder;
  final LoadingErrorWidgetBuilder errorWidget;
  final bool cache;
  final Duration fadeOutDuration;
  final Curve fadeOutCurve;
  final Duration fadeInDuration;
  final Curve fadeInCurve;
  final double width, height;
  final BoxFit fit;
  final Alignment alignment;
  final ImageRepeat repeat;
  final bool matchTextDirection;
  final Map<String, String> httpHeaders;
  final bool useOldImageOnUrlChange;
  final Color color;
  final FilterQuality filterQuality;
  final BlendMode colorBlendMode;
  final Duration placeholderFadeInDuration;

  const CachedImage({
    Key key,
    @required this.src,
    this.imageBuilder,
    this.placeholder,
    this.errorWidget,
    this.cache = true,
    this.fadeOutDuration = const Duration(milliseconds: 1000),
    this.fadeOutCurve = Curves.easeOut,
    this.fadeInDuration = const Duration(milliseconds: 500),
    this.fadeInCurve = Curves.easeIn,
    this.width,
    this.height,
    this.fit = BoxFit.cover,
    this.alignment = Alignment.center,
    this.repeat = ImageRepeat.noRepeat,
    this.matchTextDirection = false,
    this.httpHeaders,
    this.useOldImageOnUrlChange: false,
    this.color,
    this.filterQuality = FilterQuality.low,
    this.colorBlendMode,
    this.placeholderFadeInDuration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      cache && (Platform.isAndroid || Platform.isIOS || Platform.isMacOS)
          ? CachedNetworkImage(
              key: key,
              imageUrl: src,
              imageBuilder: imageBuilder,
              placeholder: placeholder,
              errorWidget: errorWidget,
              fadeOutDuration: fadeOutDuration,
              fadeOutCurve: fadeOutCurve,
              fadeInDuration: fadeInDuration,
              fadeInCurve: fadeInCurve,
              width: width,
              height: height,
              fit: fit,
              alignment: alignment,
              repeat: repeat,
              matchTextDirection: matchTextDirection,
              httpHeaders: httpHeaders,
              useOldImageOnUrlChange: useOldImageOnUrlChange,
              color: color,
              filterQuality: filterQuality,
              colorBlendMode: colorBlendMode,
              placeholderFadeInDuration: placeholderFadeInDuration,
              cacheManager: _ImageCacheManager(),
            )
          : Image.network(
              src,
              key: key,
              width: width,
              height: height,
              fit: fit,
            );
}

class _ImageCacheManager extends BaseCacheManager {
  static const key = 'imageCache';

  static _ImageCacheManager _instance;

  factory _ImageCacheManager() {
    if (_instance == null) _instance = _ImageCacheManager._();

    return _instance;
  }

  _ImageCacheManager._()
      : super(
          key,
          maxAgeCacheObject: const Duration(days: 14),
        );

  @override
  Future<String> getFilePath() async {
    final directory = await getTemporaryDirectory();
    return path.join(directory.path, key);
  }
}
