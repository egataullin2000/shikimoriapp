import 'package:flutter/foundation.dart';
import 'package:injector/injector.dart';
import 'package:intl/intl.dart';
import 'package:shiki_app/core/localization/app_localizations.dart';
import 'package:shiki_app/core/models/anime_full.dart';
import 'package:shiki_app/core/models/anime_preview.dart';
import 'package:shiki_app/core/services/api/api_service.dart';

class AnimeDetailsModel extends ChangeNotifier {
  ApiService _api;
  final AppLocalizations localizations;
  final int animeId;
  final Title title;
  final String kind;
  final String posterPreviewUrl;
  final double rating;
  bool _isLoading = true;
  String posterUrl;
  String description;
  final Map<String, String> details;
  List<Screenshot> screenshots = [null, null];
  List<Video> videos = [null, null];

  AnimeDetailsModel(AnimePreview initialData, this.localizations)
      : _api = Injector.appInstance.getDependency<ApiService>(),
        animeId = initialData.id,
        title = initialData.title,
        kind = initialData.kind,
        posterPreviewUrl = initialData.posterPreviewUrl,
        rating = initialData.score,
        description = localizations.loadingMessage,
        details = {
          localizations.animeGenres: localizations.loadingMessage,
          localizations.animeStatus: localizations.loadingMessage,
          localizations.animeEpisodesCount: localizations.loadingMessage,
          localizations.animePublishingDates: localizations.loadingMessage,
          localizations.animeStudio: localizations.loadingMessage
        } {
    _api.getAnime(initialData.id).then(_setData);
  }

  bool get isLoading => _isLoading;

  void _setData(AnimeFull data) {
    _isLoading = false;

    posterUrl = data.posterUrl;
    description = data.description;

    details[localizations.animeGenres] = data.genres.join(', ');
    details[localizations.animeStatus] = data.status.ru.toString();
    details[localizations.animeEpisodesCount] = data.episodesCount.toString();
    details[localizations.animeStudio] = data.studios.join(', ');

    var publishingDates = '';
    final dateFormatter = DateFormat('dd.MM.yyyy');
    if (data.releasedOn != null && data.airedOn != null)
      publishingDates += '${localizations.animePublishingFrom} ';
    if (data.airedOn != null)
      publishingDates += '${dateFormatter.format(data.airedOn)}';
    if (data.airedOn != null && data.releasedOn != null) publishingDates += ' ';
    if (data.releasedOn != null)
      publishingDates += '${localizations.animePublishingTo} '
          '${dateFormatter.format(data.releasedOn)}';
    details[localizations.animePublishingDates] = publishingDates;

    screenshots = data.screenshots;
    videos = data.videos;

    notifyListeners();
  }
}
