import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:shiki_app/core/localization/app_localizations.dart';
import 'package:shiki_app/core/models/anime_full.dart';
import 'package:shiki_app/ui/anime_details/anime_details_model.dart';
import 'package:shiki_app/ui/widgets/cached_image.dart';
import 'package:shiki_app/ui/widgets/expandable_text.dart';
import 'package:shiki_app/ui/widgets/material_hero.dart';
import 'package:shiki_app/ui/widgets/themed_shimmer.dart';
import 'package:styled_widget/styled_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class AnimeDetailsPage extends StatelessWidget {
  static const routeName = '/anime';

  const AnimeDetailsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Theme(
      data: Theme.of(context).copyWith(
        textTheme: textTheme.apply(
          bodyColor: textTheme.bodyText2.color.withOpacity(.87),
        ),
      ),
      child: Scaffold(
        body: AnimeDetails(),
      ),
    );
  }
}

class AnimeDetails extends StatelessWidget {
  static const _expandedAppBarHeight = 400.0;
  static const _landscapePosterHeight = 256.0;
  static const _descriptionMaxLines = 5;
  static const _screenshotItemWidth = 160.0;
  static const _screenshotItemHeight = 90.0;
  static const _videoItemWidth = 160.0;
  static const _videoItemHeight = 90.0;

  const AnimeDetails({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedScrollView(
      headerSliverBuilder: (_, innerBoxIsScrolled) =>
          [_buildAppBar(context, forceElevated: innerBoxIsScrolled)],
      body: _buildBody(context),
    );
  }

  Widget _buildAppBar(BuildContext context, {bool forceElevated = false}) {
    final isPortrait =
        MediaQuery
            .of(context)
            .orientation == Orientation.portrait;
    return SliverAppBar(
      title: isPortrait ? null : _buildTitle(context),
      expandedHeight: isPortrait ? _expandedAppBarHeight : null,
      forceElevated: forceElevated,
      pinned: true,
      flexibleSpace: isPortrait
          ? FlexibleSpaceBar(
        title: Text(
            Provider
                .of<AnimeDetailsModel>(context, listen: false)
                .title
                .ru,
            overflow: TextOverflow.ellipsis),
        background: _buildPoster(context),
      )
          : null,
    );
  }

  Widget _buildPoster(BuildContext context) {
    final isPortrait =
        MediaQuery
            .of(context)
            .orientation == Orientation.portrait;
    final height = isPortrait ? null : _landscapePosterHeight;
    return Consumer<AnimeDetailsModel>(
      builder: (_, model, preview) {
        var poster = (model.isLoading
            ? preview
            : CachedImage(
          src: model.posterUrl,
          fadeInDuration: Duration.zero,
          placeholder: (_, __) => preview,
          height: height,
        ));
        return Hero(
          tag: '${model.animeId}_poster',
          child: !isPortrait
              ? poster.clipRRect(all: 8)
              : Stack(fit: StackFit.expand, children: [
            poster,
            Container(
              decoration: const BoxDecoration(
                gradient: const LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: const [
                    Colors.black54,
                    const Color(0x00000000),
                    const Color(0x00000000),
                    Colors.black54
                  ],
                  stops: const [0, .25, .5, 1],
                ),
              ),
            )
          ]),
        );
      },
      child: _buildPosterPreview(context, height: height),
    );
  }

  Widget _buildPosterPreview(BuildContext context,
      {double width, double height}) =>
      CachedImage(
        src: Provider
            .of<AnimeDetailsModel>(context, listen: false)
            .posterPreviewUrl,
        fadeInDuration: Duration.zero,
        fadeOutDuration: Duration.zero,
        width: width,
        height: height,
      );

  Widget _buildTitle(BuildContext context) =>
      Text(Provider
          .of<AnimeDetailsModel>(context, listen: false)
          .title
          .ru);

  Widget _buildBody(BuildContext context) {
    final isPortrait =
        MediaQuery
            .of(context)
            .orientation == Orientation.portrait;
    final description = _buildDescription(context);
    final details = _buildDetails(context);
    return ListView(
      padding: const EdgeInsets.symmetric(vertical: 16),
      children: [
        Column(children: [
          isPortrait
              ? description
              : Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            _buildPoster(context),
            const SizedBox(width: 16),
            details.expanded(),
          ]),
          isPortrait ? const Divider() : SizedBox(height: 16),
          isPortrait ? details.padding(vertical: 16) : description,
          const Divider(),
        ]).padding(horizontal: 16),
        _buildMediaContent(context),
      ],
    );
  }

  Widget _buildDescription(BuildContext context) =>
      Consumer<AnimeDetailsModel>(
          builder: (_, model, __) =>
              ExpandableText(
                text: model.description,
                maxLines: _descriptionMaxLines,
                textDecorator:
                model.isLoading ? (text) => ThemedShimmer(child: text) : null,
                style: Theme
                    .of(context)
                    .textTheme
                    .bodyText2,
              ));

  Widget _buildDetails(BuildContext context) {
    final localizations = AppLocalizations.of(context);
    final model = Provider.of<AnimeDetailsModel>(context, listen: false);
    return Consumer<AnimeDetailsModel>(
      builder: (_, model, child) =>
          Column(
              children: model.details.entries
                  .map((detail) =>
              Detail(
                name: detail.key,
                value: detail.value,
                valueDecorator: model.isLoading
                    ? (valueWidget) => ThemedShimmer(child: valueWidget)
                    : null,
              ) as Widget)
                  .toList()
                ..insert(0, child)
                ..expand((detail) => [detail, const SizedBox(height: 8)])
                    .toList(growable: false)),
      child: Column(children: [
        Detail(
          name: localizations.animeRating,
          valueBuilder: () => _buildRating(context),
        ),
        const SizedBox(height: 8),
        Detail(
          name: localizations.animeKind,
          value: model.kind,
          valueDecorator: (valueWidget) =>
              MaterialHero(
                tag: '${model.animeId}_kind',
                child: valueWidget,
              ),
        ),
        const SizedBox(height: 8),
        Detail(
            name: localizations.animeTitleEn,
            value: model.title.en,
            valueDecorator: (valueWidget) =>
                MaterialHero(
                  tag: '${model.animeId}_title_en',
                  child: valueWidget,
                )),
      ]),
    );
  }

  Widget _buildRating(BuildContext context) =>
      Consumer<AnimeDetailsModel>(
        builder: (_, model, __) =>
            Hero(
              tag: '${model.animeId}_rating',
              child: RatingBarIndicator(
                itemBuilder: (context, index) =>
                const Icon(
                  Icons.star_border,
                  color: Colors.amber,
                ),
                itemCount: 5,
                itemSize: 24,
                unratedColor: Theme
                    .of(context)
                    .disabledColor,
                rating: model.rating / 2,
              ),
            ),
      );

  Widget _buildMediaContent(BuildContext context) =>
      Consumer<AnimeDetailsModel>(
        builder: (_, model, __) =>
            Column(children: [
              _buildScreenshots(context, model.screenshots),
              _buildVideos(context, model.videos)
            ]),
      );

  Widget _buildScreenshots(BuildContext context, List<Screenshot> screenshots) {
    const placeholder = ThemedShimmer(
        width: _screenshotItemWidth, height: _screenshotItemHeight);
    return _buildMediaItem(context,
        name: AppLocalizations
            .of(context)
            .animeScreenshots,
        items: screenshots
            .map((screenshot) =>
            (screenshot == null
                ? placeholder
                : CachedImage(
              src: screenshot.previewUrl,
              width: _screenshotItemWidth,
              height: _screenshotItemHeight,
              placeholder: (_, __) => placeholder,
            ))
                .card(shape: BeveledRectangleBorder()))
            .toList(growable: false));
  }

  Widget _buildVideos(BuildContext context, List<Video> videos) {
    const placeholder =
    ThemedShimmer(width: _videoItemWidth, height: _videoItemHeight);
    return _buildMediaItem(context,
        name: AppLocalizations
            .of(context)
            .animeVideos,
        items: videos
            .map((video) =>
            InkWell(
              child: Stack(fit: StackFit.passthrough, children: [
                (video == null
                    ? placeholder
                    : CachedImage(
                  src: video.posterSrc,
                  width: _videoItemWidth,
                  height: _videoItemHeight,
                  placeholder: (_, __) => placeholder,
                )),
                Container(
                  width: _videoItemWidth,
                  height: _videoItemHeight,
                  child: Icon(Icons.play_circle_outline, size: 48),
                  decoration: const BoxDecoration(
                    gradient: RadialGradient(
                      colors: [
                        Colors.black54,
                        const Color(0x00000000),
                      ],
                      stops: const [0, .5],
                    ),
                  ),
                ),
              ]).card(shape: const RoundedRectangleBorder()),
              onTap: () async =>
              await launch(
                video.url,
                forceSafariVC: false,
                forceWebView: false,
              ),
            ))
            .toList(growable: false));
  }

  ExpansionTile _buildMediaItem(BuildContext context,
      {@required String name, @required List<Widget> items}) =>
      ExpansionTile(
        title: Text(name,
            style: Theme
                .of(context)
                .textTheme
                .bodyText2
                .copyWith(fontWeight: FontWeight.w600)),
        expandedAlignment: Alignment.topLeft,
        children: [Wrap(children: items).padding(horizontal: 8)],
      );
}

class Detail extends StatelessWidget {
  final String name;
  final String value;
  final Widget Function() valueBuilder;
  final Widget Function(Widget valueWidget) valueDecorator;

  const Detail({Key key,
    @required this.name,
    this.value,
    this.valueBuilder,
    this.valueDecorator});

  @override
  Widget build(BuildContext context) {
    final textStyle = Theme
        .of(context)
        .textTheme
        .bodyText2;
    final valueWidget = valueBuilder?.call() ?? Text(value, style: textStyle);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(name, style: textStyle.copyWith(fontWeight: FontWeight.w600))
            .expanded(),
        (valueDecorator?.call(valueWidget) ?? valueWidget).expanded(),
      ],
    );
  }
}
