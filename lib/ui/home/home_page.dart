import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shiki_app/ui/catalog/catalog_page.dart';
import 'package:shiki_app/ui/home/home_model.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      body: AnimatedSwitcher(
        duration: kThemeAnimationDuration,
        child: Consumer<HomeModel>(
          builder: (_, model, child) => _buildPage(model.selectedPage),
        ),
      ),
      bottomNavigationBar: CurvedNavigationBar(
        color: theme.primaryColor,
        backgroundColor: theme.backgroundColor,
        buttonBackgroundColor: Colors.transparent,
        height: 56,
        animationDuration: kThemeAnimationDuration,
        onTap: (i) =>
        Provider.of<HomeModel>(context, listen: false).selectedPage = i,
        items: [
          _buildBottomNavBarItem(theme, Icons.movie),
          _buildBottomNavBarItem(theme, Icons.history),
          _buildBottomNavBarItem(theme, Icons.schedule),
          _buildBottomNavBarItem(theme, Icons.subscriptions),
          _buildBottomNavBarItem(theme, Icons.person),
        ],
      ),
    );
  }

  Widget _buildPage(int selectedPage) {
    switch (selectedPage) {
      case 0:
        return CatalogPage();
      default:
        return Container();
    }
  }

  Widget _buildBottomNavBarItem(ThemeData theme, IconData icon) =>
      Icon(icon, color: theme.primaryIconTheme.color);
}
