import 'package:flutter/foundation.dart';

class HomeModel extends ChangeNotifier {
  int _selectedPage = 0;

  int get selectedPage => _selectedPage;

  set selectedPage(int page) {
    _selectedPage = page;
    notifyListeners();
  }
}
